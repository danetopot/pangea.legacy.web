﻿using FTCWebApp.Caching;
using FTCWebApp.Classes;
using FTCWebApp.DataModels;
using FTCWebApp.ServiceReference1;
using FTCWebApp.Utillities;
using FTCWebApp.ViewModels;

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace FTCWebApp.Controllers
{
    public class MarkLocationForUpdateController : Controller
    {
        private FieldServicesClient _svc;
        private static List<MarkLocationForUpdateModel> displayDataList = new List<MarkLocationForUpdateModel>();

        public MarkLocationForUpdateController()
        {
            _svc = new FieldServicesClient("BasicHttpsBinding_IFieldServices");
        }

        public ActionResult Index()
        {
            UserInfo uInfo = SessionManager.Get<UserInfo>("UserInfo");
            if (uInfo == null)
            {
                return RedirectToAction("SessionReset", "Home");
            }

            MarkLocationForUpdateVM _model = new MarkLocationForUpdateVM
                (
                Classes.User_Session_Defaults.enum_Display_Page.Mark_Location_For_Update, 
                Session
                );

            _model.ActionType = string.Empty;
            _model.MarkLocationForUpdateRecord = new MarkLocationForUpdateModel();
            _model.currentUser = uInfo;
            _model.MarkLocationForUpdateRecord.NumLeadDays = 30;
            _model.Declines = new List<DeclineDetail>();
            displayDataList = new List<MarkLocationForUpdateModel>();

            SessionManager.Remove<MarkLocationForUpdateModel>("RecordToUpdate");

            return View("MarkLocationForUpdate", _model);
        }

        [HttpPost]
        public string GetMarkLocationForUpdateData(string countryCodeID, string locationCodeID)
        {
            UserInfo uInfo = SessionManager.Get<UserInfo>("UserInfo");

            displayDataList = new List<MarkLocationForUpdateModel>();

            try
            {

                User_Session_Defaults.Set_User_Preferred_MarkLocationForUpdate_Action_Session_Var(Session, "View Site");

                if (!String.IsNullOrEmpty(locationCodeID))
                {

                    if (locationCodeID.ToLower().Equals("null"))
                        locationCodeID = String.Empty; 
                    
                    User_Session_Defaults.Set_User_Preferred_Location_Code_ID_Session_Var(Session, locationCodeID);

                }

                if (!String.IsNullOrEmpty(countryCodeID))
                {

                    if (countryCodeID.ToLower().Equals("null"))
                        countryCodeID = String.Empty;

                    User_Session_Defaults.Set_User_Preferred_Country_Session_Var(Session, countryCodeID);

                }

                List<MarkLocationForUpdateValue> _retValues = _svc.GetLocationForUpdate(uInfo.ToString(), locationCodeID, countryCodeID).ToList();

                foreach (MarkLocationForUpdateValue locForUpdate in _retValues)
                {
                    MarkLocationForUpdateModel data = new MarkLocationForUpdateModel();

                    string the_LocationCalendarID = string.IsNullOrEmpty(locForUpdate.LocationCalendarID) ? string.Empty : locForUpdate.LocationCalendarID;

                    data.CountryName = locForUpdate.CountryName;

                    data.CountryCodeID = locForUpdate.CountryCodeID;

                    data.ScheduleUpdateDate = locForUpdate.VisitDate.ToShortDateString();

                    data.CompletionDate = locForUpdate.DueDate.ToShortDateString();

                    data.LocationCodeID = locForUpdate.LocationCodeID;

                    string the_location_HTML = 
                        "<a href=\"/MarkLocationForUpdate/EditMarkLocationForUpdate?markLocID=" 
                        + the_LocationCalendarID 
                        + "\" style=\"color:Blue;font-weight:bold;\">" 
                        + locForUpdate.LocationName.Trim() 
                        + "</a>";

                    data.LocationName = the_location_HTML;  

                    data.NumLeadDays = string.IsNullOrEmpty(locForUpdate.LeadDays) ? 0 : int.Parse(locForUpdate.LeadDays);

                    data.LocCalendarID = the_LocationCalendarID;

                    displayDataList.Add(data);
                }

                SessionManager.Set<List<MarkLocationForUpdateModel>>("RecordsToUpdate", displayDataList);
            }
            catch (Exception ex)
            {
                displayDataList = new List<MarkLocationForUpdateModel>();

                Log.Debug(ex.Message);
            }

            return JsonConvert.SerializeObject(displayDataList);
        }

        public string UpdateCompletionDate(string visitDate, int dueDays)
        {
            if (!string.IsNullOrEmpty(visitDate))
            {
                DateTime _tempVisit = Convert.ToDateTime(visitDate);
                return _tempVisit.AddDays(dueDays).ToShortDateString();
            }
            else
                return string.Empty;
        }

        public ActionResult CancelUpdateMarkForLocation(string markLocID)
        {
            MarkLocationForUpdateVM _model = new MarkLocationForUpdateVM();
            _model.currentUser = SessionManager.Get<UserInfo>("UserInfo");
            _model.Declines = new List<DeclineDetail>();
            _model.MarkLocationForUpdateRecord = new MarkLocationForUpdateModel();
            ReturnMessage _retMsg = _svc.CancelLocationForUpdate(_model.currentUser.ToString(), markLocID);

            if (_retMsg.Description.ToLower().Contains("successful"))
                _model.DisplayMessage = "Canceled Scheduled Location Update";
            else
                _model.DisplayMessage = "Error canceling Scheduled Location Update";

            return View("MarkLocationForUpdate", _model);
        }

        public ActionResult AddMarkLocationForUpdate(MarkLocationForUpdateVM model)
        {

            User_Session_Defaults.Set_User_Preferred_MarkLocationForUpdate_Action_Session_Var
                (
                Session,
                "Add Site"
                );


            MarkLocationForUpdateVM _model = new MarkLocationForUpdateVM
                (
                Classes.User_Session_Defaults.enum_Display_Page.Mark_Location_For_Update, 
                Session
                );
             

            _model.currentUser = SessionManager.Get<UserInfo>("UserInfo");
            _model.Declines = new List<DeclineDetail>();

            string locCalendarID = model.MarkLocationForUpdateRecord.LocCalendarID;
            string locationCodeID = model.MarkLocationForUpdateRecord.LocationCodeID;
            DateTime CompletionDate = Convert.ToDateTime(model.MarkLocationForUpdateRecord.CompletionDate);
            int numLeadDays = model.MarkLocationForUpdateRecord.MarkImmediately ? 0 : model.MarkLocationForUpdateRecord.NumLeadDays;
            DateTime scheduleUpdateDate = Convert.ToDateTime(model.MarkLocationForUpdateRecord.ScheduleUpdateDate);

            if (scheduleUpdateDate < DateTime.Today.AddDays(-365))
            {
                _model.DisplayMessage = "A valid date is required to mark a location for update!";
                return View("MarkLocationForUpdate", _model);
            }

            ReturnMessage retMesg;
            if (model.EditMode)
                retMesg = _svc.EditLocationForUpdate(_model.currentUser.ToString(), locationCodeID, Convert.ToInt32(locCalendarID), scheduleUpdateDate, numLeadDays, CompletionDate);
            else
                retMesg = _svc.MarkLocationForUpdate(_model.currentUser.ToString(), locationCodeID, scheduleUpdateDate, numLeadDays, CompletionDate);

            if (retMesg.Message != "Success")
                _model.DisplayMessage = retMesg.Message;
            else
                _model.DisplayMessage = string.Format("{0} successfully marked for Update.", model.MarkLocationForUpdateRecord.LocationCodeID);

            return View("MarkLocationForUpdate", _model);
        }

        public ActionResult UpdateMarkLocationForUpdate(FormCollection formData)
        {

            User_Session_Defaults.Set_User_Preferred_MarkLocationForUpdate_Action_Session_Var
                (
                Session,
                "Edit Site"
                );


            ReturnMessage retMesg;
            MarkLocationForUpdateVM _model = new MarkLocationForUpdateVM
                (
                Classes.User_Session_Defaults.enum_Display_Page.Mark_Location_For_Update,
                Session
                );
            _model.currentUser = SessionManager.Get<UserInfo>("UserInfo");
            _model.Declines = new List<DeclineDetail>();

            MarkLocationForUpdateModel record = SessionManager.Get<MarkLocationForUpdateModel>("RecordToUpdate");
            SessionManager.Remove<MarkLocationForUpdateModel>("RecordToUpdate");

            if (!string.IsNullOrEmpty(record.LocCalendarID))
            {
                //TODO Update to pass only the completion data once API is updated 
                retMesg = _svc.EditLocationForUpdate(_model.currentUser.ToString(), record.LocationCodeID, Convert.ToInt32(record.LocCalendarID), Convert.ToDateTime(formData["MarkLocationForUpdateRecord.ScheduleUpdateDate"]), record.NumLeadDays, Convert.ToDateTime(formData["MarkLocationForUpdateRecord.ScheduleUpdateDate"]).AddDays(Convert.ToDouble(formData["MarkLocationForUpdateRecord.DaysToComplete"])));
                if (retMesg.Message != "Success")
                    _model.DisplayMessage = retMesg.Message;
                else
                    _model.DisplayMessage = string.Format("{0}({1}) successfully Updated.", record.LocationName, record.LocationCodeID);
            }
            else
                _model.DisplayMessage = "Error Updating the Record";

            return View("MarkLocationForUpdate", _model);
        }

        public ActionResult EditMarkLocationForUpdate(string markLocID)
        {
            var LocationList = SessionManager.Get<List<MarkLocationForUpdateModel>>("RecordsToUpdate");
            MarkLocationForUpdateVM _model = new MarkLocationForUpdateVM(
                Classes.User_Session_Defaults.enum_Display_Page.Mark_Location_For_Update,
                Session
                );
            _model.currentUser = SessionManager.Get<UserInfo>("UserInfo");
            _model.MarkLocationForUpdateRecord = new MarkLocationForUpdateModel();
            _model.Declines = new List<DeclineDetail>();
            _model.MarkLocationForUpdateRecord = LocationList.FirstOrDefault(a => a.LocCalendarID == markLocID);
            _model.ActionType = "Edit Site";
            _model.EditMode = true;
            SessionManager.Set<MarkLocationForUpdateModel>("RecordToUpdate", _model.MarkLocationForUpdateRecord);
            SessionManager.Remove<List<MarkLocationForUpdateModel>>("RecordsToUpdate");

            return View("MarkLocationForUpdate", _model);
        }
    }
}