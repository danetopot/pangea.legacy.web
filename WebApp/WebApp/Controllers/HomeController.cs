﻿using FTCWebApp.ViewModels;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using FTCWebApp.FeedServiceReference;
using System.Configuration;
using FTCWebApp.Caching;
using FTCWebApp.DataModels;
using System.Net;
using System.Collections.Generic;
using log4net;
using FTCWebApp.ServiceReference1;
using System;

namespace FTCWebApp.Controllers
{
    public class HomeController : Controller
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(HomeController));
        private FeedServiceClient _feedSvc;

        public HomeController()
        {
            _feedSvc = new FeedServiceClient("BasicHttpsBinding_IFeedService");
        }

        public ActionResult Home()
        {
            HomeVM model = new HomeVM();
            model.Declines = new List<DeclineDetails>();

            if (Request.IsAuthenticated || (User != null && User.Identity.IsAuthenticated))
            {
                try
                {
                    var userClaims = User.Identity as System.Security.Claims.ClaimsIdentity;
                    model.currentUser.FullName = userClaims?.FindFirst("name")?.Value;
                    model.currentUser.AccountName = userClaims?.Name;
                    model.currentUser.ID = new Guid(userClaims?.FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier")?.Value);


                    UserPermissions _userPerm = new UserPermissions();
                    _userPerm.AppName = model.currentUser.AppName;
                    _userPerm.AppShortName = model.currentUser.ShortAppName;
                    _userPerm.UserID = model.currentUser.ID.ToString();

                    _userPerm = _feedSvc.GetUserPermissions(_userPerm);

                    model.currentUser.AddPermissions(_userPerm.Permissions);

                    SessionManager.Set<UserInfo>("UserInfo", model.currentUser);

                    log.Debug("User Info Collected" + model.currentUser.AccountName);
                }
                catch (Exception ex)
                {
                    log.Debug(ex.Message);
                }
            }
            else
                log.Debug("User not Authenticated");

            log.Debug("Leaving Home");
            return View(model);
        }

        public ActionResult SessionReset()
        {
            HomeVM model = new HomeVM();
            model.Declines = new List<DeclineDetails>();

            return View("Home", model);
        }

        public ActionResult Reports()
        {
            return Redirect(ConfigurationManager.AppSettings["ReportRedirect"]);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        /// <summary>
        /// Send an OpenID Connect sign-in request.
        /// Alternatively, you can just decorate the SignIn method with the [Authorize] attribute
        /// </summary>
        public void SignIn()
        {
            try
            {
                // Send an OpenID Connect sign-in request.
                HttpContext.GetOwinContext().Authentication.Challenge(new AuthenticationProperties { RedirectUri = ConfigurationManager.AppSettings["redirectUri"].ToString() }, OpenIdConnectAuthenticationDefaults.AuthenticationType);

                log.Debug("SignIn");
            }
            catch (Exception ex)
            {
                log.Debug(ex.Message);
            }
        }

        /// <summary>
        /// Send an OpenID Connect sign-out request.
        /// </summary>
        public void SignOut()
        {
            try
            {
                HttpContext.GetOwinContext().Authentication.SignOut(OpenIdConnectAuthenticationDefaults.AuthenticationType, CookieAuthenticationDefaults.AuthenticationType);
                log.Debug("Leaving SingOut");
            }
            catch (Exception ex)
            {
                log.Debug(ex.Message);
            }
        }

        public void Endsession()
        {
            HttpContext.GetOwinContext().Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
        }
    }
}