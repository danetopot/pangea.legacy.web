﻿using FTCWebApp.Caching;
using FTCWebApp.Classes;
using FTCWebApp.DataModels;
using FTCWebApp.ServiceReference1;
using FTCWebApp.Utillities;
using FTCWebApp.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace FTCWebApp.Controllers
{
    public class ApplicationManagerController : Controller
    {
        private FieldServicesClient _svc = new FieldServicesClient("BasicHttpsBinding_IFieldServices");
        private static List<ApplicationManagerModel> displayDataList = new List<ApplicationManagerModel>();


        // GET: ApplicationManager
        public ActionResult Index()
        {
            UserInfo uInfo = SessionManager.Get<UserInfo>("UserInfo");
            if (uInfo == null)
            {
                return RedirectToAction("SessionReset", "Home");
            }

            ApplicationManagerVM model = 
                new ApplicationManagerVM
                (
                    User_Session_Defaults.enum_Display_Page.ApplicationManager,
                    Session
                    );

            model.currentUser = uInfo;
            model.Declines = new List<DeclineDetail>();
            displayDataList = new List<ApplicationManagerModel>();
            model.ActionType = String.Empty;

            return View("ApplicationManager", model);
        }

        /// <summary>
        ///Get the data for the selected code table 
        /// </summary>
        [HttpPost]
        public string GetCodeTableData(string codeTableName)
        {
            var uInfo = SessionManager.Get<UserInfo>("UserInfo");

            displayDataList = new List<ApplicationManagerModel>();

            List<CodeTableValue> codeTableDataList = _svc.GetCodeTableValues(codeTableName, uInfo.ToString()).ToList();

            foreach (var row in codeTableDataList)
            {
                ApplicationManagerModel data = new ApplicationManagerModel();

                if (row.Active)
                {
                    data.Active = "Yes";
                }
                else
                {
                    data.Active = "No";
                }

                data.CodeID = row.CodeID.ToString();
                // In Location code is maps to Location Name

                data.ColumnWithSameName = row.Code;

                data.Description = row.Description;

                data.PublicDescription = row.PublicDescription;

                data.CodeTableName = codeTableName;

                if (
                    codeTableName == "Chore" || 
                    codeTableName == "Favorite_Activity" || 
                    codeTableName == "Favorite_Learning" ||
                    codeTableName == "Grade_Level" || 
                    codeTableName == "Country"
                    )
                {
                    if (row.MinAge > 0)
                        data.MinAge = row.MinAge / 365;
                }
                if (codeTableName == "Country")
                {
                    //region code id is the parent id on country, country name maps to description
                    data.ParentCodeID = row.ParentCodeID.ToString();

                    data.DefaultLanguage = row.PublicDescription;
                }
                if (codeTableName == "Location")
                {
                    data.SponsorshipSite = row.Description;

                    data.DefaultLanguage = row.MinAge.ToString();

                    string parentID = row.ParentCodeID.ToString();

                    var counturies = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Country"));

                    bool Is_Country_Found = counturies.Where(xx => xx.Item1 == parentID).Any();

                    if (Is_Country_Found)
                    {
                        //get the country name from the cache based on the countryid returned from the API 
                        data.ParentCodeID = counturies.FirstOrDefault(a => a.Item1 == parentID).Item2;
                    }
                    else
                    {
                        continue;
                    }
                    
                }
                if (codeTableName == "Content_Type")
                {
                    if (row.MinAge == 0)
                    {
                        data.Required = "No";
                    }
                    if (row.MinAge == 1)
                    {
                        data.Required = "Yes";
                    }
                }
                if (codeTableName == "Region")
                {
                    data.DefaultLanguage = row.MinAge.ToString();
                }

                displayDataList.Add(data);
            }

            return JsonConvert.SerializeObject(displayDataList);
        }

        /// <summary>
        /// Gets called to edit the selected row of the code table
        /// </summary>
        /// <param name="id"> is as string containg selected row's code id and tablename seperated by '|'</param>
        public ActionResult EditCodeTable(string id)
        {

            ApplicationManagerVM model = new ApplicationManagerVM(User_Session_Defaults.enum_Display_Page.ApplicationManager, Session);

            model.currentUser = SessionManager.Get<UserInfo>("UserInfo");

            model.Declines = new List<DeclineDetail>();

            model.codeTableRecord = new ApplicationManagerModel();

            if (!String.IsNullOrEmpty(id))
            {
                try
                {
                    var recordInfo = id.Split('|');

                    model.codeTableRecord = displayDataList.FirstOrDefault(a => a.CodeID == recordInfo[0] && a.CodeTableName == recordInfo[1]);

                    model.TableName = recordInfo[1];

                    if (model.codeTableRecord.Active == "Yes")
                    {
                        model.codeTableRecord.Active = "true";
                    }
                    else
                    {
                        model.codeTableRecord.Active = "false";
                    }
                    var countries = (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Country"));
                    model.codeTableRecord.ParentCountryCodeID = countries.FirstOrDefault(a => a.Item2 == model.codeTableRecord.ParentCodeID).Item1;

                }
                catch (Exception ex)
                {
                    model.DisplayMessage = Helper.MessageWithDateTimeAdded("unable to process request, exception occured " + ex.Message);
                }

                model.ActionType = "Edit Data";
            }

            return View("AddEditCodeTable", model);
        }

        /// <summary>
        /// Called when adding new records to the code table 
        /// </summary>
        /// <returns>redirects to the view to add edit code table </returns>
        public ActionResult AddCodeTable(FormCollection formCollection)
        {
            ApplicationManagerVM model = new ApplicationManagerVM(User_Session_Defaults.enum_Display_Page.ApplicationManager, Session);

            model.currentUser = SessionManager.Get<UserInfo>("UserInfo");

            model.Declines = new List<DeclineDetail>();

            model.codeTableRecord = new ApplicationManagerModel();

            model.TableName = model.codeTableRecord.CodeTableName = formCollection.Get("TableName");

            model.codeTableRecord.Active = "Yes";

            model.ActionType = formCollection.Get("ActionType");

            return View("AddEditCodeTable", model);
        }

        /// <summary>
        /// Submit the changes to either add new values or edit exisisting values in code tables to the API
        /// </summary>
        [HttpPost]
        public ActionResult Submit(ApplicationManagerVM model)
        {

            TryUpdateModel(model);

            string codeTableName = model.TableName;

            ApplicationManagerModel codeTableRecord = model.codeTableRecord;

            model.currentUser = Session["UserInfo"] as UserInfo;
            
            CodeTableValue value = new CodeTableValue();

            // If editing exisiting pass the codeID to the API, if it is an add API generated a new code id 
            if (model.ActionType == "Edit Data")
            {
                value.CodeID = int.Parse(codeTableRecord.CodeID);
            }

            value.CodeTable = codeTableName;

            value.Active = bool.Parse(codeTableRecord.Active);

            // In case of country Description maps to Country Name and Public Descp maps to Default_Laanguage
            value.Description = codeTableRecord.Description;

            value.PublicDescription = codeTableRecord.PublicDescription;

            // code is the columnwithsamename and its value for that column is same as description
            value.Code = codeTableRecord.Description;

            switch (codeTableName)
            {
                case "Chore":
                case "Favorite_Activity":
                case "Favorite_Learning":
                case "Grade_Level":
                    // case "Country":

                    // In case of country min_age maps to min school age
                    value.MinAge = codeTableRecord.MinAge * 365;

                    break;

                case "Content_Type":

                    if (codeTableRecord.Required == "Yes")
                    {
                        //Min maps to required in content_type
                        value.MinAge = 1;
                    }
                    else
                    {
                        value.MinAge = 0;
                    }

                    break;

                case "Country":

                    // In case of country min_age maps to min school age
                    value.MinAge = codeTableRecord.MinAge * 365;

                    // This maps to Region Code ID
                    value.ParentCodeID = int.Parse(codeTableRecord.ParentCodeID);

                    // This code maps to Country Code
                    value.Code = codeTableRecord.ColumnWithSameName;

                    break;

                case "Location":

                    // This maps to Region Code ID
                    //value.ParentCodeID = int.Parse(codeTableRecord.ParentCodeID);

                    // This code maps to Country Code
                    value.ParentCodeID = Convert.ToInt32(codeTableRecord.ParentCountryCodeID);


                    string the_location_Y_N = codeTableRecord.Description;

                     bool Is_SponsorshipSite = the_location_Y_N == "1";
                     string Is_SponsorshipSite2 = codeTableRecord.SponsorshipSite;

                    int Location_Code_ID = Convert.ToInt32(value.CodeID);

                    value.CodeID = Location_Code_ID;

                    value.PublicDescription = null;

                    //Descrition maps to Sponsorship site in API
                    //value.Description = Is_SponsorshipSite? "1" : "0"; // codeTableRecord.Description;
                    //value.SponsorshipSite = Is_SponsorshipSite;
                    value.MinAge = codeTableRecord.MinAge;
                    value.Code = codeTableRecord.ColumnWithSameName;
                    string parentID = codeTableRecord.CodeID.ToString();

                    //value.ParentCodeID = Convert.ToInt32(parentID);

                    value.Active = codeTableRecord.Active.ToLower() == "false"? false : true;
                    value.SponsorshipSite = codeTableRecord.SponsorshipSite.ToLower() == "false" ? false : true;
                    value.Description = value.SponsorshipSite? "1" : "0";
                    break;

                case "Region":

                    value.Code = codeTableRecord.ColumnWithSameName;

                    value.MinAge = int.Parse(codeTableRecord.PublicDescription);

                    break;

                case "Decline_Reason":

                    /*
                    if (model.ActionType == "Edit Data")
                    {
                        value.ParentCodeID = Convert.ToInt32(codeTableRecord.CodeID);
                    }
                    else
                    {  */                  
                        value.ParentCodeID = Convert.ToInt32(codeTableRecord.ParentCodeID);
                    //}

                    break;

            }

            ReturnMessage messageFromAPI = new ReturnMessage();

            try
            {
                messageFromAPI = _svc.SaveCodeValue(value, model.currentUser.ToString());
            }
            catch (Exception ex)
            {
                messageFromAPI.Message = ex.Message;
            }

            if (messageFromAPI.Message.Contains("Success"))
            {
                if (model.ActionType == "Add New Code")
                {
                    model.DisplayMessage = Helper.MessageWithDateTimeAdded("Successfuly added data  to code table " + codeTableName);
                }
                else
                {
                    model.DisplayMessage = Helper.MessageWithDateTimeAdded("Successfuly edited data for row with code id " + value.CodeID + " to code table " + codeTableName);
                }
            }

            displayDataList = new List<ApplicationManagerModel>();

            return View("ApplicationManager", model);
        }

        /// <summary>
        /// Return to the parent page
        /// </summary>
        /// <returns></returns>
        public ActionResult Cancel()
        {
            return RedirectToAction("Index", "ApplicationManager");
        }



        [HttpPost]
        public void SetActionTypePreference(string ActionTypePreference)
        {

            User_Session_Defaults.Set_User_Preferred_ApplicationManager_Action_Session_Var(Session, ActionTypePreference);

        }

    }
}