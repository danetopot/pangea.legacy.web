﻿using FTCWebApp.Caching;
using FTCWebApp.DataModels;
using FTCWebApp.ServiceReference1;
using FTCWebApp.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace FTCWebApp.Controllers
{
    public class ReportsController : Controller
    {
        // GET: Reports
        public ActionResult Index()
        {
            UserInfo uInfo = SessionManager.Get<UserInfo>("UserInfo");
            if (uInfo == null)
            {
                return RedirectToAction("SessionReset", "Home");
            }

            CommonVM model = new CommonVM();
            model.currentUser = uInfo;
            model.Declines = new List<DeclineDetails>();
            return View(model);
        }
    }
}