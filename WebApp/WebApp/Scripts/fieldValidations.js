﻿//This function initializes the third party controls for chrores, favorite activities
$(document).ready(function () {
    $('.chosen-select').chosen({ max_selected_options: $('#hiddenRange_ChoreCodeID').prop('max'), width: '100%', placeholder_text_single: "Select some options..." });
});

//This function sets default values for the grade and chores fields based on country in update module 
$(function () {
    if ($('#module_name').val() == "Update") {
        var selectedDate = $("#child_DateOfBirth").val();
        var CountryId = $("#child_CountryId").val();
        var hiddenId = $("#hiddenMinSchoolAge_" + CountryId);
        var age = calculateAge(selectedDate);
        if ($('#child_GradeLevelCodeID').val() == "") {
            if ((age <= hiddenId.val() / 365)) {
                $('#child_GradeLevelCodeID option').filter(function () {
                    return $(this).html() == "Too Young for School";
                }).prop('selected', true);
            }
            else {
                $('#child_GradeLevelCodeID option').filter(function () {
                    return $(this).html() == "Too Young for School";
                }).prop('selected', false);
            }
        }
        if ($('#child_ChoreCodeID').val() == "") {
            if ((age <= ($("#hiddenDefaultString_ChoreCodeID").prop('max')) / 365) && ($("#hiddenDefaultString_ChoreCodeID").prop('placeholder') != null)) {
                $('#child_ChoreCodeID option').filter(function () {
                    return $(this).html() == $("#hiddenDefaultString_ChoreCodeID").prop('placeholder').toString();
                }).prop('selected', true);
                $('#child_ChoreCodeID').trigger("chosen:updated");
            }
            else {
                $('#child_ChoreCodeID option').filter(function () {
                    return $(this).html() == $("#hiddenDefaultString_ChoreCodeID").prop('placeholder').toString();
                }).prop('selected', false);
                $('#child_ChoreCodeID').trigger("chosen:updated");
            }
        }
    }
});

//This function calculates the age based on date of birth entered by the user
function calculateAge(birthday) {
    var today = new Date();
    var birthdate = new Date(birthday);
    var age = today.getFullYear() - birthdate.getFullYear();
    var m = today.getMonth() - birthdate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthdate.getDate())) {
        age--;
    }
    return age;
}

// This function sets the 'Required' field labels to be visible based on the values contained in hidden fields.
$(function () {
    if ($('#hiddenReq_FirstName').data('isrequired') == "True") {
        $('#FirstName_reqIndicator').css("visibility", "visible");
        $('#FirstName_info').css("visibility", "visible");
    }
    if ($('#hiddenReq_LastName').data('isrequired') == "True") {
        $('#LastName_reqIndicator').css("visibility", "visible");
        $('#LastName_info').css("visibility", "visible");
    }
    if ($('#hiddenReq_GenderCodeID').data('isrequired') == "True") {
        $('#GenderCodeID_reqIndicator').css("visibility", "visible");
        $('#GenderCodeID_info').css("visibility", "visible");
    }
    if ($('#hiddenReq_DateOfBirth').data('isrequired') == "True") {
        $('#DateOfBirth_reqIndicator').css("visibility", "visible");
        $('#DateOfBirth_info').css("visibility", "visible");
    }
    if ($('#hiddenReq_DisabilityStatusCodeID').data('isrequired') == "True") {
        $('#DisabilityStatusCodeID_reqIndicator').css("visibility", "visible");
        $('#DisabilityStatusCodeID_info').css("visibility", "visible");
    }

    if ($('#hiddenReq_GradeLevelCodeID').data('isrequired') == "True") {
        $('#GradeLevelCodeID_reqIndicator').css("visibility", "visible");
        $('#GradeLevelCodeID_info').css("visibility", "visible");
    }
    if ($('#hiddenReq_NumberOfBrothers').data('isrequired') == "True") {
        $('#NumberofSiblings_reqIndicator').css("visibility", "visible");
        $('#NumberofSiblings_info').css("visibility", "visible");
    }

    if ($('#hiddenReq_NumberOfSisters').data('isrequired') == "True") {
        $('#NumberofSiblings_reqIndicator').css("visibility", "visible");
        $('#NumberofSiblings_info').css("visibility", "visible");
    }
    if ($('#hiddenReq_PersonalityTypeCodeID').data('isrequired') == "True") {
        $('#PersonalityTypeCodeID_reqIndicator').css("visibility", "visible");
        $('#PersonalityTypeCodeID_info').css("visibility", "visible");
    }
    if ($('#hiddenReq_ChoreCodeID').data('isrequired') == "True") {
        $('#ChoreCodeID_reqIndicator').css("visibility", "visible");
        $('#ChoreCodeID_info').css("visibility", "visible");
        $('#ChoreCodeID_info').prop('title', 'Chore Code ID is required\n Select number of values between ' + $("#hiddenRange_ChoreCodeID").prop('min') + ' and ' + $("#hiddenRange_ChoreCodeID").prop('max'));
    }
    if ($('#hiddenReq_FavoriteActivityCodeID').data('isrequired') == "True") {
        $('#FavoriteActivityCodeID_reqIndicator').css("visibility", "visible");
        $('#FavoriteActivityCodeID_info').css("visibility", "visible");
        $('#FavoriteActivityCodeID_info').prop('title', 'Favorite Activity ID is required\n Select number of values between ' + $("#hiddenRange_FavoriteActivityCodeID").prop('min') + ' and ' + $("#hiddenRange_FavoriteActivityCodeID").prop('max'));
    }
    if ($('#hiddenReq_FavoriteLearningCodeID').data('isrequired') == "True") {
        $('#FavoriteLearningCodeID_reqIndicator').css("visibility", "visible");
        $('#FavoriteLearningCodeID_info').css("visibility", "visible");
    }
    if ($('#hiddenReq_LivesWithCodeID').data('isrequired') == "True") {
        $('#LivesWithCodeID_reqIndicator').css("visibility", "visible");
        $('#LivesWithCodeID_info').css("visibility", "visible");
    }
    if ($('#hiddenReq_MajorLifeEvent').data('isrequired') == "True") {
        $('#MajorLifeEvent_reqIndicator').css("visibility", "visible");
        $('#MajorLifeEvent_info').css("visibility", "visible");
    }

})