﻿using FTCWebApp.DAL;
using FTCWebApp.ServiceReference1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FTCWebApp.ViewModels.GUI_Pieces
{
    public class GUI_Pieces_Helper
    {


        public static List<SelectListItem> Get_Users_Country_Drop_Down_List_Items(string the_user_id)
        {

            API_EndPointer api_endpointer = new API_EndPointer();

            Country_DropDown_Item[] country_select_items = api_endpointer.svc.GetCountryRecords(the_user_id);

            List<SelectListItem> l_sli = new List<SelectListItem>();

            country_select_items
                .ToList()
                .ForEach
                (
                xx =>
                {
                    l_sli.Add
                    (
                        new SelectListItem()
                        {
                            Text = xx.Country_Name,
                            Value = xx.Country_ID.ToString()
                        }
                        );
                }
                );

            ;

            return l_sli;

        }


        public static List<SelectListItem> Get_Country_Location_Entry_Drop_Down_List_Items
            (
            string the_user_id,
            int country_ID
            )
        {

            API_EndPointer api_endpointer = new API_EndPointer();

            DropDown_Item_w_ID[] location_select_items =
                api_endpointer.svc.Get_Locations_For_User
                (
                    the_user_id,
                    country_ID
                );

            List<SelectListItem> l_sli = new List<SelectListItem>();

            location_select_items
                .ToList()
                .ForEach
                (
                xx =>
                {
                    l_sli.Add
                    (
                        new SelectListItem()
                        {
                            Text = xx.DropDown_Caption,
                            Value = xx.ID.ToString()
                        }
                        );
                }
                );

            return l_sli;

        }


        public static List<SelectListItem> Get_DeclineReasons_Drop_Down_List_Items(string the_user_id)
        {

            API_EndPointer api_endpointer = new API_EndPointer();

            Decline_Reason_Codes[] DeclineReasonCodes_select_items = api_endpointer.svc.DeclineReasonCodes(the_user_id);

            List<SelectListItem> l_sli = new List<SelectListItem>();

            DeclineReasonCodes_select_items
                .Where(xx => xx.Active == true)
                .OrderBy(xx => xx.Description)
                .ToList()
                .ForEach
                (
                xx =>
                {
                    l_sli.Add
                    (
                        new SelectListItem()
                        {
                            Text = xx.Description,
                            Value = xx.Decline_Reason_Code_ID.ToString()
                        }
                        );
                }
                );

            ;

            return l_sli;

        }




    }

}