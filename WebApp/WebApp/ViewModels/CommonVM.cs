﻿using System.Collections.Generic;
using System.Linq;
using FTCWebApp.DataModels;
using System.Web.Mvc;
using FTCWebApp.Caching;
using FTCWebApp.ServiceReference1;
using System;
using System.ComponentModel.DataAnnotations;
using log4net;

using System.Web;
using FTCWebApp.Classes;

using FTCWebApp.ViewModels.GUI_Pieces;

namespace FTCWebApp.ViewModels
{
    //ViewModel for the Enrollment Page
    public class CommonVM
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CommonVM));
        private SelectList locationCodes;
        private SelectList locations;


        public CommonVM()
        {
            CommonVM_Constructor();
        }


        public CommonVM
            (
            FTCWebApp.Classes.User_Session_Defaults.enum_Display_Page e_Display_Page,
            HttpSessionStateBase hsb
            )
        {

            CommonVM_Constructor();

            user_Session_Defaults = new User_Session_Defaults
                (
                e_Display_Page,
                hsb
                );

        }


        private void CommonVM_Constructor()
        {

            currentUser = new UserInfo();

            AlreadyUploadedDocuments = new List<DownLoadFile>();

            Declines = new List<DeclineDetails>();

        }

        #region CHILD
        //Field Data
        public Child child { get; set; }
        public IEnumerable<SelectListItem> Countries
        {
            get
            {
                try
                {

                    string the_user_id = currentUser.ID.ToString();

                    List<SelectListItem> l_sli = GUI_Pieces_Helper.Get_Users_Country_Drop_Down_List_Items(the_user_id);

                    return l_sli;

                }
                catch (Exception ex)
                {
                    log.Debug(ex.Message);
                    return Enumerable.Empty<SelectListItem>();
                }
            }
        }

        public IEnumerable<SelectListItem> QCWorkflows
        {
            get
            {
                try
                {
                    return (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Workflow")).Where(a => a.Item3 == "True").Select(c => new SelectListItem() { Text = c.Item2, Value = c.Item1 });
                }
                catch (Exception ex)
                {
                    log.Debug(ex.Message);
                    return Enumerable.Empty<SelectListItem>();
                }
            }
        }

        public SelectList Locations
        {
            get
            {
                if (locations == null)
                {
                    locations = new SelectList(Enumerable.Empty<SelectListItem>());
                }

                return locations;
            }
        }

        public SelectList LocationCodes
        {
            get
            {
                if (locationCodes == null)
                {
                    locationCodes = new SelectList(Enumerable.Empty<SelectListItem>());
                }

                return locationCodes;
            }
        }

        public IEnumerable<SelectListItem> Genders
        {
            get
            {
                return (CacheManager.GetFromCache<List<SelectListItem>>("Gender"));
            }
        }
        public IEnumerable<SelectListItem> Disabalitites { get; set; } = Disabilites();
        public int? Brothers { get; set; }
        public int? Sisters { get; set; }

        private IEnumerable<SelectListItem> _personalities;
        public IEnumerable<SelectListItem> Personalities
        {
            get
            {
                if (_personalities == null)
                    _personalities = (CacheManager.GetFromCache<List<SelectListItem>>("Personality_Type"));

                return _personalities;
            }
            set
            {
                _personalities = value;
            }
        }
        private IEnumerable<SelectListItem> _livesWith;
        public IEnumerable<SelectListItem> LivesWith
        {
            get
            {
                if (_livesWith == null)
                    _livesWith = (CacheManager.GetFromCache<List<SelectListItem>>("Lives_With"));
                return _livesWith;
            }
            set
            {
                _livesWith = value;
            }
        }

        private IEnumerable<SelectListItem> _requiredContentTypes;
        public IEnumerable<SelectListItem> RequiredContentTypes
        {
            get
            {
                if (_requiredContentTypes == null)
                    _requiredContentTypes = (CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type")).Where(m => m.Required == true).Select(c => new SelectListItem() { Text = c.Content_Type, Value = c.Content_Type_Code_ID.ToString() });

                return _requiredContentTypes;
            }
            set
            {
                if (_requiredContentTypes != value)
                {
                    _requiredContentTypes = value;
                }
            }
        }

        // update derives its required documents from column rules
        public IEnumerable<SelectListItem> RequiredContentTypesUpdate
        {
            get
            {
                var rules = CacheManager.GetFromCache<List<FieldRuleModel>>("Column_Rules").SelectMany(c => c.ColumnRules).Where(m => (m.validationType == "ReqValidation" || m.validationType == "DocValidation") && m.ModuleName == "Update" && m.isRequired).ToList();
                var req_docs = new List<SelectListItem>();
                var all_docs = (CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type")).Select(c => new { ContentType = "ContentType_" + c.Content_Type_Code_ID.ToString(), ContentName = c.Description }).ToList();

                foreach (var item in all_docs)
                {
                    if (rules.FirstOrDefault(a => a.ColumnRuleName.Contains(item.ContentName)) != null)
                    {
                        req_docs.Add(new SelectListItem { Value = item.ContentType.Split('_')[1], Text = item.ContentName });
                    }
                }
                return req_docs.AsEnumerable();
            }
            set { this.RequiredContentTypes = value; }
        }
        public IEnumerable<SelectListItem> OptionalContentTypes
        {
            get
            {
                return (CacheManager.GetFromCache<List<ContentTypeModel>>("Content_Type")).Where(m => m.Required == false).Select(c => new SelectListItem() { Text = c.Content_Type, Value = c.Content_Type_Code_ID.ToString() });
            }
        }
        public FileModel uploadDocument { get; set; }
        public static List<SelectListItem> Disabilites() // This method populates  data for Disability Field
        {
            List<SelectListItem> disabilites = new List<SelectListItem>();
            disabilites.Add(new SelectListItem { Value = "0", Text = "No" });
            disabilites.Add(new SelectListItem { Value = "1", Text = "Yes" });
            return disabilites;
        }
        #endregion

        #region ENROLLMENT
        public IEnumerable<SelectListItem> FavLearning { get; set; }
        public IEnumerable<SelectListItem> Grades { get; set; }
        public IEnumerable<SelectListItem> ChoresatHome { get; set; }
        public IEnumerable<SelectListItem> FavActivities { get; set; }
        public IEnumerable<SelectListItem> RemoveReasons
        {
            get
            {
                return (CacheManager.GetFromCache<List<SelectListItem>>("Child_Remove_Reason"));
            }
        }
        //show create and declien as the two actions on enrollment
        public IEnumerable<SelectListItem> Enrollment_ActionTypes
        {
            get
            {
                try
                {
                    return (CacheManager.GetFromCache<List<Tuple<string, string, string, string, string>>>("Action")).Where(a => a.Item3 == "True").Select(c => new SelectListItem() { Text = c.Item2, Value = c.Item1 });
                }
                catch (Exception ex)
                {
                    log.Debug(ex.Message);
                    return Enumerable.Empty<SelectListItem>();
                }
            }
        }
        //Updates page child history data
        public CommonVM ChildHistory { get; set; }
        #endregion

        #region UPDATE
        //major life event for update history
        public string MajorLifeEvent { get; set; }
        public IEnumerable<SelectListItem> Update_ActionTypes
        {
            get
            {
                try
                {
                    return (CacheManager.GetFromCache<List<Tuple<string, string, string, string, string>>>("Action")).Where(a => a.Item4 == "True").Select(c => new SelectListItem() { Text = c.Item2, Value = c.Item1 });
                }
                catch (Exception ex)
                {
                    log.Debug(ex.Message);
                    return Enumerable.Empty<SelectListItem>();
                }
            }
        }
        #endregion

        #region QUALITY CONTROL
        //public IEnumerable<SelectListItem> QC_WorkflowTypes
        //{
        //    get
        //    {
        //        return (CacheManager.GetFromCache<List<SelectListItem>>("Workflow"));
        //    }
        //}
        public WorkFlowStep[] Child_workflowsteps { get; set; }
        public IEnumerable<SelectListItem> Child_workflowteps_ddl { get; set; }
        public IEnumerable<SelectListItem> QC_Decline_SubReason { get; set; }

        [Display(Name = "Approve")]
        public string ApproveWorkflowStep { get; set; }

        [Display(Name = "Translation Required")]
        public string IsTranslationRequired { get; set; }

        public string IsTranslationRequiredByDefault { get; set; }

        public IEnumerable<SelectListItem> IsTranslationRequiredDD
        {
            get
            {
                var list = new List<SelectListItem>();
                list.Add(new SelectListItem() { Value = "true", Text = "Yes" });
                list.Add(new SelectListItem() { Value = "false", Text = "No" });
                return list;
            }
        }

        public IEnumerable<SelectListItem> QCDeclineSubList
        {
            get
            {
                return (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Decline_SubReason")).Select(c => new SelectListItem() { Text = c.Item3, Value = c.Item1 }).ToList();
            }
        }
        #endregion

        public IEnumerable<SelectListItem> ActionReasons
        {
            get
            {
                return (CacheManager.GetFromCache<List<SelectListItem>>("Action_Reason"));
            }
        }
        //Client side validations for enrollment and update. Required validations for showing markers on fields for enrollment and update.
        public List<SelectListItem> MinSchoolAgeList
        {
            get
            {
                return (CacheManager.GetFromCache<List<Tuple<string, string, string>>>("Country")).Select(c => new SelectListItem() { Text = c.Item1, Value = c.Item3 }).ToList();
            }
        }
        public List<ColumnRuleModel> ClientValidationRules
        {
            get
            {
                return (CacheManager.GetFromCache<List<FieldRuleModel>>("Column_Rules")).SelectMany(c => c.ColumnRules).Where(m => m.validationType == "ClientValidation" && m.ModuleName == "Enrollment").ToList();
            }
        }
        public List<ColumnRuleModel> Update_ClientValidationRules
        {
            get
            {
                return (CacheManager.GetFromCache<List<FieldRuleModel>>("Column_Rules")).SelectMany(c => c.ColumnRules).Where(m => m.validationType == "ClientValidation" && m.ModuleName == "Update").ToList();
            }
        }
        public List<ColumnRuleModel> ReqValidationRules
        {
            get
            {
                return (CacheManager.GetFromCache<List<FieldRuleModel>>("Column_Rules")).SelectMany(c => c.ColumnRules).Where(m => m.validationType == "ReqValidation" && m.ModuleName == "Enrollment").ToList();
            }
        }
        public List<ColumnRuleModel> Update_ReqValidationRules
        {
            get
            {
                return (CacheManager.GetFromCache<List<FieldRuleModel>>("Column_Rules")).SelectMany(c => c.ColumnRules).Where(m => m.validationType == "ReqValidation" && m.ModuleName == "Update").ToList();
            }
        }
        public List<TranslationStep> Child_translationSteps { get; set; }
        public List<DownLoadFile> AlreadyUploadedDocuments { get; set; }
        // List of workflow steps declined from the child 
        public List<DeclineDetails> Declines { get; set; }
        public IEnumerable<SelectListItem> Child_declinedSteps_ddl { get; set; }
        public IEnumerable<SelectListItem> declinedContnetTypes { get; set; }
        public UserInfo currentUser { get; set; }

        public String DisplayMessage { get; set; }


        public User_Session_Defaults user_Session_Defaults;

    }
}