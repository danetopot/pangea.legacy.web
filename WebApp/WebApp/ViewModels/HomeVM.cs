﻿using FTCWebApp.DataModels;
using FTCWebApp.ServiceReference1;
using System;
using System.Collections.Generic;

namespace FTCWebApp.ViewModels
{
    public class HomeVM
    {
        public HomeVM()
        {
            Declines = new List<DeclineDetails>();
            currentUser = new UserInfo();
            DisplayMessage = String.Empty;
        }
        public List<DeclineDetails> Declines { get; set; }
        public UserInfo currentUser { get; set; }
        public String DisplayMessage { get; set; }
    }
}