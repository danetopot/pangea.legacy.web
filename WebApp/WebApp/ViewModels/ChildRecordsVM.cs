﻿using FTCWebApp.Caching;
using FTCWebApp.Classes;
using FTCWebApp.DAL;
using FTCWebApp.DataModels;
using FTCWebApp.ServiceReference1;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using FTCWebApp.ViewModels.GUI_Pieces;


namespace FTCWebApp.ViewModels
{
    public class ChildRecordsVM
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ChildRecordsVM));

        public ChildRecordsVM()
        {
            ChildRecordsVM_Constructor();
        }

        public ChildRecordsVM
            (
            FTCWebApp.Classes.User_Session_Defaults.enum_Display_Page e_Display_Page,
            HttpSessionStateBase hsb
            )
        {
            ChildRecordsVM_Constructor();

            user_Session_Defaults = new User_Session_Defaults
                (
                e_Display_Page,
                hsb
                );

        }
               


        private void ChildRecordsVM_Constructor()
        {
            Declines = new List<DeclineDetails>();
        }

        public Child child { get; set; }

        public IEnumerable<SelectListItem> Countries
        {
            get
            {
                try
                {
                    ;

                    string the_user_id = currentUser.ID.ToString();

                    List<SelectListItem> l_sli = Countries_Given_User_ID(the_user_id);

                    return l_sli;

                }
                catch (Exception ex)
                {
                    log.Debug(ex.Message);

                    return Enumerable.Empty<SelectListItem>();
                }
            }
        }


        public static List<SelectListItem> Countries_Given_User_ID(string the_user_id)
        {
            return GUI_Pieces_Helper.Get_Users_Country_Drop_Down_List_Items(the_user_id);
        }


        public static List<SelectListItem> DeclineReasons_Given_User_ID(string the_user_id)
        {
            return GUI_Pieces_Helper.Get_DeclineReasons_Drop_Down_List_Items(the_user_id);
        }


        public IEnumerable<SelectListItem> ChildRecords_ActionTypes
        {
            get
            {
                return (CacheManager.GetFromCache<List<Tuple<string, string, string, string, string>>>("Action").Select(c => new SelectListItem() { Text = c.Item2, Value = c.Item1 }));
            }
        }

        public UserInfo currentUser { get; set; }

        public List<DeclineDetails> Declines { get; set; }

        public String DisplayMessage { get; set; }


        
        public User_Session_Defaults user_Session_Defaults;

    }
}