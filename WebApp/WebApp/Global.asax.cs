﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using FTCWebApp.Caching;
using System.Net;
using System.Net.Security;
using log4net;
using System;

namespace FTCWebApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(MvcApplication));
        void Application_Error(Object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError().GetBaseException();
            log.Error("App_Error", ex);
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            log4net.Config.XmlConfigurator.Configure();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //ServicePointManager.ServerCertificateValidationCallback = (sender, cert, chain, error) =>
            //{
            //    var request = sender as HttpWebRequest;
            //    if (request != null && request.Address.Host == "pangea.staging.feedthechildren.org")
            //        return true;
            //    else
            //        return error == SslPolicyErrors.RemoteCertificateChainErrors;
            //};
        }
    }
}
