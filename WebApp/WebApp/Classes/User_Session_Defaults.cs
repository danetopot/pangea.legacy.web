﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FTCWebApp.Classes
{
    public class User_Session_Defaults
    {

        public User_Session_Defaults
            (
            enum_Display_Page _e_Display_Page,
            HttpSessionStateBase hsb
            )
        {

            e_Display_Page = _e_Display_Page;

            Dictionary<string, object> dct_User_Defaults = Get_User_Defaults(hsb);

            _dct_Session_Items = Get_Session_Dictionary(hsb);

            Set_User_Default_Value
                (
                Utillities.Helper.Session_Key_Data_Entry_Default_Country,
                ref Selected_User_Country_Value
                );


            Set_User_Default_Value
                (
                Utillities.Helper.Session_Key_Data_Entry_Default_Location_Code,
                ref Selected_User_Location_Code_Value
                );


            Set_User_Default_Value
                (
                Utillities.Helper.Session_Key_Data_Entry_Default_QualityControl_Action_Type,
                ref Selected_User_QualityControl_Action_Type_Value
                );


            Set_User_Default_Value
                (
                Utillities.Helper.Session_Key_Data_Entry_Default_Enrollment_Action_Type,
                ref Selected_User_Enrollment_Action_Type_Value
                );

            Set_User_Default_Value
                (
                Utillities.Helper.Session_Key_Data_Entry_Default_Mark_Location_For_Update_Action_Type,
                ref Selected_User_Mark_Location_For_Update_Action_Type_Value
                );

            Set_User_Default_Value
                (
                Utillities.Helper.Session_Key_Data_Entry_Default_Update_Action_Type,
                ref Selected_User_Update_Action_Type_Value
                );


            Set_User_Default_Value
                (
                Utillities.Helper.Session_Key_Data_Entry_Default_Translation_Action_Type,
                ref Selected_User_Translation_Action_Type_Value
                );



            Set_User_Default_Value
                (
                Utillities.Helper.Session_Key_Data_Entry_Default_ApplicationManager_Action_Type,
                ref Selected_User_ApplicationManager_Action_ID_Value
                );

            switch (e_Display_Page)
            {
                case enum_Display_Page.Mark_Location_For_Update:

                    Country_ID = "MarkLocationForUpdateRecord_CountryCodeID";

                    Location_DropDown_List_ID = "MarkLocationForUpdateRecord_LocationName";

                    break;
            }

        }


        private Dictionary<string, object> _dct_Session_Items = new Dictionary<string, object>();


        public enum enum_Display_Page
        {
            Child_Records,
            Enrollment,
            Mark_Location_For_Update,
            Update,
            Quality_Control,
            Translation,
            Shipping_Shipping_Creation,
            Shipping_Shipping_Recieving, 
            ApplicationManager
        }

        public enum_Display_Page e_Display_Page;



        public string Country_ID = "child_CountryId";

        public string Selected_User_Country_Value;




        public string Submit_Button_ID = "";



        public string Location_DropDown_List_ID = "child_LocationName";

        public string Selected_User_Location_Code_Value;



        public string QualityControl_Action_Type_ID = "child_WorkflowId";

        public string Selected_User_QualityControl_Action_Type_Value;




        public string Enrollment_Action_Type_ID = "Enrollment_ActionTypes";

        public string Selected_User_Enrollment_Action_Type_Value;




        public string Selected_User_Mark_Location_For_Update_Action_ID = "ActionType";

        public string Selected_User_Mark_Location_For_Update_Action_Type_Value = "";



        public string Selected_User_Update_Action_ID = "Update_ActionTypes";

        public string Selected_User_Update_Action_Type_Value = "";



        public string Selected_User_Translation_Action_ID = "ActionReason";

        public string Selected_User_Translation_Action_Type_Value = "";



        public string Selected_User_ApplicationManager_Action_ID = "ActionType";

        public string Selected_User_ApplicationManager_Action_ID_Value = "";




        private void Set_User_Default_Value(string Session_Key, ref string the_Value)
        {

            if (_dct_Session_Items.ContainsKey(Session_Key))
            {
                try
                {
                    the_Value = _dct_Session_Items[Session_Key].ToString();
                }
                catch
                {

                }

            }

        }





        public static Dictionary<string, object> Get_Session_Dictionary(HttpSessionStateBase hsb)
        {

            Dictionary<string, object> dct_Session_Items = new Dictionary<string, object>();

            for (int i = 0; i < hsb.Contents.Count; i++)
            {

                dct_Session_Items.Add(hsb.Keys[i], hsb[i]);

            }

            return dct_Session_Items;

        }


        public static void Set_User_Preferred_Country_Session_Var
            (
            HttpSessionStateBase hsb, 
            string Country_ID
            )
        {

            Set_Session_Value_If_Exists_Else_Remove
                (
                hsb,
                Utillities.Helper.Session_Key_Data_Entry_Default_Country,
                Country_ID
                );

        }


        public static void Set_User_Preferred_Enrollment_Action_Type_Session_Var
            (
            HttpSessionStateBase hsb,
            string Enrollment_Action_Type
            )
        {

            Set_Session_Value_If_Exists_Else_Remove
                (
                hsb,
                Utillities.Helper.Session_Key_Data_Entry_Default_Enrollment_Action_Type,
                Enrollment_Action_Type
                );

        }



        public static void Set_User_Preferred_Default_QualityControl_Action_Type_Session_Var
            (
            HttpSessionStateBase hsb,
            string Default_QualityControl_Action_Type
            )
        {

            Set_Session_Value_If_Exists_Else_Remove
                (
                hsb,
                Utillities.Helper.Session_Key_Data_Entry_Default_QualityControl_Action_Type,
                Default_QualityControl_Action_Type
                );

        }


        public static void Set_User_Preferred_Location_Code_ID_Session_Var
            (
            HttpSessionStateBase hsb,
            string Location_Code_ID
            )
        {

            Set_Session_Value_If_Exists_Else_Remove
                (
                hsb,
                Utillities.Helper.Session_Key_Data_Entry_Default_Location_Code,
                Location_Code_ID
                );

        }


        public static void Set_User_Preferred_MarkLocationForUpdate_Action_Session_Var
            (
            HttpSessionStateBase hsb, 
            string MarkLocationForUpdate_Action
            )
        {

            Set_Session_Value_If_Exists_Else_Remove
                (
                hsb,
                Utillities.Helper.Session_Key_Data_Entry_Default_Mark_Location_For_Update_Action_Type,
                MarkLocationForUpdate_Action
                );

        }



        public static void Set_User_Preferred_Update_Action_Session_Var
            (
            HttpSessionStateBase hsb, 
            string Update_Action
            )
        {

            Set_Session_Value_If_Exists_Else_Remove
                (
                hsb,
                Utillities.Helper.Session_Key_Data_Entry_Default_Update_Action_Type,
                Update_Action
                );

        }


        public static void Set_User_Preferred_Translation_Action_Session_Var
            (
            HttpSessionStateBase hsb, 
            string Translation_Action
            )
        {

            Set_Session_Value_If_Exists_Else_Remove
                (
                hsb,
                Utillities.Helper.Session_Key_Data_Entry_Default_Translation_Action_Type,
                Translation_Action
                );

        }

        public static void Set_User_Preferred_ApplicationManager_Action_Session_Var
            (
            HttpSessionStateBase hsb,
            string ApplicationManager_Action
            )
        {

            Set_Session_Value_If_Exists_Else_Remove
                (
                hsb,
                Utillities.Helper.Session_Key_Data_Entry_Default_ApplicationManager_Action_Type,
                ApplicationManager_Action
                );

        }



        private static void Set_Session_Value_If_Exists_Else_Remove
            (
            HttpSessionStateBase hsb,
            string Session_Var_Key,
            string the_value
            )
        {

            if (the_value != "")
            {
                hsb[Session_Var_Key] = the_value;
            }
            else
            {
                hsb.Remove(Session_Var_Key);
            }

        }



        public static Dictionary<string, object> Get_User_Defaults(HttpSessionStateBase hsb)
        {

            Dictionary<string, object> dct_Session_Items = new Dictionary<string, object>();

            List<string> stemps = new List<string>() 
            {
                Utillities.Helper.Session_Key_Data_Entry_Default_Country,
                Utillities.Helper.Session_Key_Data_Entry_Default_Location_Code,
                Utillities.Helper.Session_Key_Data_Entry_Default_QualityControl_Action_Type,
                Utillities.Helper.Session_Key_Data_Entry_Default_Enrollment_Action_Type,
                Utillities.Helper.Session_Key_Data_Entry_Default_Mark_Location_For_Update_Action_Type,
                Utillities.Helper.Session_Key_Data_Entry_Default_Update_Action_Type,
                Utillities.Helper.Session_Key_Data_Entry_Default_Translation_Action_Type,
                Utillities.Helper.Session_Key_Data_Entry_Default_ApplicationManager_Action_Type
            };
            
            stemps.ForEach
                (
                xx =>
                {
                    Add_User_Defaults
                    (
                        dct_Session_Items,
                        xx,
                        hsb[xx]
                        );
                }
                );

            return dct_Session_Items;

        }


        private static void Add_User_Defaults
            (
            Dictionary<string, object> dct_Session_Items,
            string the_key_name,
            object the_value
            )
        {
            if (the_value != null)
            {
                dct_Session_Items.Add(the_key_name, the_value);
            }
        }



        public void Add_Dictionary_Items_To_Session
            (
            HttpSessionStateBase hsb,
            Dictionary<string, object> dct_Session_Items
            )
        {

            dct_Session_Items.ToList().ForEach
                (
                xx =>
                {

                    hsb[xx.Key] = xx.Value;

                    if (xx.Key != "UserInfo")
                    {
                        _dct_Session_Items[xx.Key] = xx.Value;
                    }

                }
                );

        }


        public bool Has_User_Location_Defaults
        {
            get
            {

                bool has_it = (
                    Selected_User_Country_Value != null &&
                    Selected_User_Location_Code_Value != null
                    );

                return has_it;

            }

        }
        
        

    }
}