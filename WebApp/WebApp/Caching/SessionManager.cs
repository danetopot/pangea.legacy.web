﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;

namespace FTCWebApp.Caching
{
    //Summary
    //This class takes care of storing user defined variables in session
    public class SessionManager
    {
        static HttpSessionState Session
        {
            get
            {
                if (HttpContext.Current == null)
                    throw new ApplicationException("No Http Context, No Session to Get!");

                return HttpContext.Current.Session;
            }
        }
        //Get a value from the session using 'key'
        public static T Get<T>(string key)
        {
            if (HttpContext.Current.Session[key] == null)
                return default(T);
            else
                return (T)HttpContext.Current.Session[key];
        }
        //Add a keyvalue pair to the session
        public static void Set<T>(string key, T value)
        {
            HttpContext.Current.Session[key] = value;
        }
        //Remove a value from the session using its 'key'
        public static void Remove<T>(string key)
        {
            HttpContext.Current.Session.Remove(key);
        }
    }
}