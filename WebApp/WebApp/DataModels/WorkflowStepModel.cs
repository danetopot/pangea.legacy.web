﻿namespace FTCWebApp.DataModels
{
    public class WorkflowStepModel
    {
        public string Description { get; set; }

        public bool TranslationRequired { get; set; }

        public int step_code_id { get; set; }
    }
}