﻿
namespace FTCWebApp.DataModels
{
    //Summary
    //This class contains the validation rules to be applied on the UI form fields on the client side
    public class ColumnRuleModel
    {
        public string ColumnRuleName { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public string ModuleName { get; set; }
        public bool isRequired { get; set; }
        public string maxValue { get; set; }
        public string minValue { get; set; }
        public string defaultString { get; set; }
        public string defaultValue { get; set; }
        public string validationType { get; set; }
    }
}