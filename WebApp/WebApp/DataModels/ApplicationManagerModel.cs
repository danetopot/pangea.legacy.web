﻿namespace FTCWebApp.DataModels
{
    public class ApplicationManagerModel
    {
        public string CodeTableName { get; set; }
        public string Active { get; set; }
        public string ColumnWithSameName { get; set; }
        public string CodeID { get; set; }
        public string Description { get; set; }
        public string PublicDescription { get; set; }
        public int MinAge { get; set; }
        public string DisplayMessage { get; set; }
        public string Required { get; set; }
        // public string ModuleType { get; set; }
        public string ParentCodeID { get; set; }
        public string ParentCountryCodeID { get; set; }
        public string DefaultLanguage { get; set; }
        public string SponsorshipSite { get; set; }
    }
}