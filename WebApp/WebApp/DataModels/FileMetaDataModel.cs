﻿
namespace FTCWebApp.DataModels
{
    public class FileMetaDataModel
    {
        public string DateTaken { get; set; }

        public string Original { get; set; }

        public string Digitized { get; set; }

        public string GPSAltitude { get; set; }

        public string GPSAltitudeRef { get; set; }

        public string GPSDateStamp { get; set; }

        public string GPSImgDirectionRef { get; set; }

        public string GPSLatitude { get; set; }

        public string GPSLatitudeRef { get; set; }

        public string GPSLongitude { get; set; }

        public string GPSLongitudeRef { get; set; }

        public string GPSTimeStamp { get; set; }

        public string GPSVersionID { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }

        public string Software { get; set; }
    }
}