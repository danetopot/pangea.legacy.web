﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FTCWebApp.DataModels
{
    public class FileModel
    {
        public string ChildID { get; set; }

        public ContentTypeModel ContentType { get; set; }

        public byte[] FileBytes { get; set; }

        public string FileName { get; set; }

        public FileTypeModel FileType { get; set; }

        public FileMetaDataModel MetaData { get; set; }

    }
}