﻿using FTCWebApp.FeedServiceReference;
using System;

namespace FTCWebApp.DataModels
{
    public class UserInfo
    {
        public UserInfo()
        {
            ID              = Guid.Empty;
            FullName        = string.Empty;
            AccountName     = string.Empty;

            CanEdit         = false;
            CanQC           = false;
            CanEnroll       = false;
            CanUpdate       = false;
            CanReport       = false;
            CanSchedule     = false;
            CanMaintenance  = false;
            CanTranslate    = false;
            CanShipping     = false;
            
        }

        public void AddPermissions(Permission[] _permissions)
        {
            //TODo add permission for shipping
            CanShipping = true;
            foreach (Permission _perm in _permissions)
            {
                switch (_perm.Name.ToLower())
                {
                    case "edit":
                        CanEdit = true;
                        break;
                    case "qc":
                        CanQC = true;
                        break;
                    case "enrollment":
                        CanEnroll = true;
                        break;
                    case "update":
                        CanUpdate = true;
                        break;
                    case "report":
                        CanReport = true;
                        break;
                    case "schedule":
                        CanSchedule = true;
                        break;
                    case "maintenance":
                        CanMaintenance = true;
                        break;
                    case "translate":
                        CanTranslate = true;
                        break;
                }
            }
        }

        public string AppName { get { return "Pangea"; } }

        public string ShortAppName { get { return "Pan"; } }

        public Guid ID { get; set; }

        public string FullName { get; set; }

        public string AccountName { get; set; }

        public bool CanEdit { get; private set; } // TODO: What is this one for????

        public bool CanQC { get; private set; }

        public bool CanEnroll { get; private set; }

        public bool CanUpdate { get; private set; }

        public bool CanReport { get; private set; }

        public bool CanSchedule { get; private set; }

        public bool CanMaintenance { get; private set; }

        public bool CanTranslate { get; private set; }

        public bool CanShipping { get; private set; }

        public override string ToString()
        {
            return ID.ToString();
        }
    }
}