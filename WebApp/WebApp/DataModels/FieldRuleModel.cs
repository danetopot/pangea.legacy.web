﻿using System.Collections.Generic;

namespace FTCWebApp.DataModels
{
    //Summary
    //This class maps the table column rules with their respective fields on the client side
    public class FieldRuleModel
    {
        public string FieldName { get; set; }
        public List<ColumnRuleModel> ColumnRules { get; set; }

    }
}