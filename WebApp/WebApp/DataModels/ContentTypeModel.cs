﻿namespace FTCWebApp.DataModels
{
    public class ContentTypeModel
    {
        public int Content_Type_Code_ID { get; set; }

        public string Content_Type { get; set; }

        public string Description { get; set; }

        public bool Required { get; set; }

        //aded these properties to use in the controllet do not like it will need to change later
        public string ContentType { get; set; }

        public string ContentName { get; set; }
    }
}