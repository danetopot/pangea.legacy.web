﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FTCWebApp.DataModels
{
    public class MarkLocationForUpdateModel
    {
        public MarkLocationForUpdateModel()
        {
            CountryName = String.Empty;
            CountryCodeID = String.Empty;
            LocationName = String.Empty;
            LocationCodeID = String.Empty;
            NumLeadDays = 30; 
            LocCalendarID = String.Empty;
            MarkImmediately = false;
            DaysToComplete = 90;
        }

        /// <summary>
        /// The Selected Country Name
        /// </summary>
        public String CountryName { get; set; }

        /// <summary>
        /// The Selected Country ID
        /// </summary>
        [Display(Name = "Country:")]
        [Required]
        public String CountryCodeID { get; set; }

        /// <summary>
        /// The Selected Location Name
        /// </summary>
        [Display(Name = "Location:")]
        [Required]
        public String LocationName { get; set; }

        /// <summary>
        /// The Selected Location Code ID
        /// </summary>
        [Display(Name = "Location Code:")]
        [Required]
        public String LocationCodeID { get; set; }

        private DateTime _scheduleUpdateDate;
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        public string ScheduleUpdateDate
        {
            get
            {
                if (_scheduleUpdateDate != null)
                    return _scheduleUpdateDate.ToShortDateString();

                return string.Empty;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                    _scheduleUpdateDate = Convert.ToDateTime(value);
                else
                    _scheduleUpdateDate = new DateTime();

            }
        }

        /// <summary>
        /// The Number of Lead Days before the Site Trip
        /// </summary>
        [Required]
        [Range(1, 365)]
        public int NumLeadDays { get; set; }

        /// <summary>
        /// The Number of Days after the Trip the Update information is Due
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public string CompletionDate
        {
            get
            {
                if (_scheduleUpdateDate != null)
                    return _scheduleUpdateDate.AddDays(DaysToComplete).ToShortDateString();

                return string.Empty;
            }
            set
            {
                DateTime _CompDate = Convert.ToDateTime(value);
                DaysToComplete = (_CompDate - _scheduleUpdateDate).Days;
            }
        }

        [Required]
        public int DaysToComplete { get; set; }

        [Display(Name = "Mark Location ID:")]
        public string LocCalendarID { get; set; }

        [Display(Name = "Immediately")]
        public bool MarkImmediately { get; set; }
    }
}