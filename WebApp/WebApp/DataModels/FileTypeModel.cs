﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FTCWebApp.DataModels
{
    public class FileTypeModel
    {
        public int File_Type_Code_ID { get; set; }

        public string File_Type { get; set; }

        public string File_Description { get; set; }

        public string File_Extension { get; set; }
    }
}