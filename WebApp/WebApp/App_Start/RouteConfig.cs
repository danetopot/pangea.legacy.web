﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FTCWebApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                name: "QCView",
                url: "PanMain/LoadQCView/{id}/{workflowID}/{pendingChildID}",
                defaults: new { controller = "PanMain", action = "LoadQCView", id = UrlParameter.Optional, workflowID = UrlParameter.Optional, pendingChildID = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Home", id = UrlParameter.Optional }
            );
        }
    }
}
